
package cruds;

import connector.ConexionObjetos;
import java.sql.ResultSet; 
import java.util.LinkedList;
import pojos.Category;

/**
 *
 * @author D M
 */
public class CRUDCategorias {
    private static ConexionObjetos conn;
    
    public CRUDCategorias(){
        try{
            conn = ConexionObjetos.getInstance();
        } catch (Exception e){
            e.printStackTrace();
        }
}
    public static LinkedList<Category> listarCategorias(){
        LinkedList<Category> listaCategorias = new LinkedList<>();
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM  category");

            while (rs.next()){
                Category categoria = new Category();
                categoria.setId(rs.getInt("id"));
                categoria.setName(rs.getString("name"));
                
                listaCategorias.add(categoria);
            }
            rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return listaCategorias;
    }
}
