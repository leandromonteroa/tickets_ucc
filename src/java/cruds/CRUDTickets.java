package cruds;

import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import patronbuilder.BuilderTicketGeneral;
import patronbuilder.BuilderTicketGraderia;
import patronbuilder.BuilderTicketPlatino;
import patronbuilder.TicketVendedor;
import pojos.Ticket;

/**
 *
 * @author D M
 */
public class CRUDTickets {
    private static ConexionObjetos conn;
    
    public CRUDTickets(){
        try{
            conn = ConexionObjetos.getInstance();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static LinkedList<Ticket> listarTickets(){
        LinkedList<Ticket> listaTickets = new LinkedList<>();
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM ticket, category WHERE ticket.category = category.id");
            while (rs.next()){
                Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setName(rs.getString("name"));
                ticket.setDescription(rs.getString("description"));
                ticket.setPrice(rs.getInt("price"));
                ticket.setCategory(rs.getInt("category"));
                
                ticket.setCategoryName(rs.getString("category.name"));
                
                listaTickets.add(ticket);
            }
            rs.close();
        }catch(Exception e){
            e.printStackTrace();        
        }
        return listaTickets;
    }
    public static LinkedList <Ticket> listarTicketsBuilder(int categoryB){
       LinkedList<Ticket> listaTicketsGeneral = new LinkedList<>();
       LinkedList<Ticket> listaTicketsGraderia = new LinkedList<>();
       LinkedList<Ticket> listaTicketsPlatino = new LinkedList<>();
       TicketVendedor pd = new TicketVendedor();
       try{
           ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM ticket "+
                   " WHERE category = " + categoryB);
           while (rs.next()){
           if(categoryB == 2){
               pd.setTicketBuilder(new BuilderTicketGeneral());
               pd.buildTicket(rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaTicketsGeneral.add(pd.getTicketPatronBuilder());
           } else if(categoryB == 4){
               pd.setTicketBuilder(new BuilderTicketGraderia());
               pd.buildTicket(rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaTicketsGraderia.add(pd.getTicketPatronBuilder()); 
           }else if(categoryB == 3){
               pd.setTicketBuilder(new BuilderTicketPlatino());
               pd.buildTicket(rs.getString("name"), rs.getString("description"), 
                       rs.getInt("price"), categoryB);
               listaTicketsPlatino.add(pd.getTicketPatronBuilder());
           }
          }
           rs.close();
       }catch(Exception e){
           e.printStackTrace();
       }        
       if (categoryB == 2){
           return listaTicketsGeneral;
       }else if(categoryB == 4){
           return listaTicketsGraderia;
       }else if (categoryB == 3){
           return listaTicketsPlatino;
       }
       return null;
    }
    
    public void insertarTicket(Ticket ticket){
        String name = ticket.getName();
        String description = ticket.getDescription();
        int price = ticket.getPrice();
        int category = ticket.getCategory();
        try{
            conn.getbConn().getSt().executeUpdate("INSERT INTO ticket (name, description, price, category)"+
                    "values ('" + name + "', '"+description+"', '"+price+"', '"+category+"')");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    // operacion para modificar un registrio en la vase de datos 
    
    public void modificarTicket(Ticket ticket){
        int id = ticket.getId();
        String name = ticket.getName();
        String description = ticket.getDescription();
        int price = ticket.getPrice();
        int category = ticket.getCategory();
        try{
            conn.getbConn().getSt().executeUpdate("UPDATE ticket SET name='" + name + "', "+
                    "description = '" + description +
                    "', price = '" + price +
                    "', category = '" + category +
                    "' WHERE id = " + id);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    // operacion para borrar un registro en la base de datos
    
    public void borrarTicket(int id){
        try{
            conn.getbConn().getSt().executeUpdate("DELETE FROM ticket WHERE id = " + id);
        }catch (Exception e){
            e.printStackTrace();
        
    }
  }
}