package testPatroness;

import connector.ConexionObjetos;
import cruds.CRUDTickets;
import static cruds.CRUDTickets.listarTickets;
import java.sql.ResultSet;
import java.util.LinkedList;
import patronfacade.FacadeTickets;
import patronfacade.TicketsFacade;
import pojos.Ticket;

/**
 *
 * @author D M
 */
public class TestPatronesFacade {
    static FacadeTickets facadeTickets;
    public static void main(String args[]) {
        facadeTickets = new FacadeTickets();
        listarTickets();
    
    }
    public static void listarTickets (){
        LinkedList<TicketsFacade> listaTicketsFacade = facadeTickets.generarTicketsFacade();
        System.out.print("---------------------------------------------------------------------------------------");
        for(int x=0; x< listaTicketsFacade.size(); x++){
        System.out.println("name ----> "+listaTicketsFacade.get(x).getNameTicket()+ " / price $ >>> "+
                listaTicketsFacade.get(x).getPriceTicket());
        TicketsFacade ticket = listaTicketsFacade.get(x);
        System.out.println("description ---> " + ticket.getDescriptionTicket()+ " / category >>> " +
                ticket.getCategoryTicket());
    }
  }
}
