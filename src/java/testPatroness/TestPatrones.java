package testPatroness;

import connector.ConexionObjetos;
import cruds.CRUDTickets;
import static cruds.CRUDTickets.listarTickets;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Ticket;

/**
 *
 * @author D M
 */
public class TestPatrones {
    static CRUDTickets crudTickets;

    public static void main(String args[]) {


        crudTickets = new CRUDTickets();
        
        listarTickets();

        Ticket ticketNueva = new Ticket();
        ticketNueva.setName("partido de futbol america vs junior");
        ticketNueva.setDescription("final de superliga");
        ticketNueva.setPrice(50000);
        ticketNueva.setCategory(2);

        crudTickets.insertarTicket(ticketNueva);
        
        listarTickets();
        
        Ticket ticketActualizada = new Ticket();
        ticketActualizada.setId(5);
        ticketActualizada.setName("partido amistoso nacional vs cali");
        ticketActualizada.setDescription("encuentro deportivo amistoso");
        ticketActualizada.setPrice(20000);
        ticketActualizada.setCategory(2);
        
        crudTickets.modificarTicket(ticketActualizada);
        
        listarTickets();
        
        crudTickets.borrarTicket(2);
        listarTickets();
        
    }
    public static void listarTickets (){
        LinkedList<Ticket> listaTickets = crudTickets.listarTickets();
        System.out.print("------------------------------------------------");
        for (int x = 0; x < listaTickets.size(); x++) {
            System.out.println("name --> " + listaTickets.get(x).getName() + "/ price $ >>>> " + listaTickets.get(x).getPrice());
            Ticket ticket = listaTickets.get(x);
            System.out.println("description --> " + ticket.getDescription() + "/ category  >>>> " + ticket.getCategoryName());
    }
    }
}
