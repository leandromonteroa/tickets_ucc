package servlets;

import com.sun.jmx.snmp.Enumerated;
import cruds.CRUDTickets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.Ticket;

/**
 *
 * @author D M
 */
@WebServlet(name = "ControladorTickets", urlPatterns = {"/ControladorTickets"})
public class ControladorTickets extends HttpServlet {
    
    CRUDTickets crudTickets;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        crudTickets = new CRUDTickets();
        // valido el dato correspondiente al id
        int id = 0;
        if(!request.getParameter("id").equals("") && request.getParameter("id") != null){
            id = Integer.parseInt(request.getParameter("id"));
        }
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        int price = Integer.parseInt(request.getParameter("price"));
        int category = Integer.parseInt(request.getParameter("category"));
        
        Enumeration<String> parameters = request.getParameterNames();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        parameters.nextElement();
        
        String operacion = parameters.nextElement();
        
        Ticket ticket = new Ticket();
        ticket.setId(id);
        ticket.setName(name);
        ticket.setDescription(description);
        ticket.setPrice(price);
        ticket.setCategory(category);
        
        // programo la toma de desiciones del controlador
        if(operacion.equals("insertarticket")){
            crudTickets.insertarTicket(ticket);
        }
        if(operacion.equals("editarticket")){
            crudTickets.modificarTicket(ticket);
            
        }
        if(operacion.equals("eliminarticket")){
            crudTickets.borrarTicket(ticket.getId());
        }
        response.sendRedirect("tickets.jsp");
     
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorTickets</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorTickets at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}