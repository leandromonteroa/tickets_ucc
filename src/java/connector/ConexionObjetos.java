package connector;

import java.sql.ResultSet;
import java.sql.SQLException;
//import javax.naming.spi.DirStateFactory;

/**
 *
 * @author D M
 */
public class ConexionObjetos {

    private BeanConnector bConn;

    private static ConexionObjetos cxObjects;

    public ConexionObjetos() {

        try {

            bConn = new BeanConnector();
            bConn.setDriver("com.mysql.jdbc.Driver");
            bConn.setUser("root");
            bConn.setPassword("root");
            bConn.setUrl("jdbc:mysql://localhost:3307/tickets?serverTimezone=UTC");
            bConn.conectar();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ConexionObjetos getInstance() {
        if (cxObjects == null) {

            cxObjects = new ConexionObjetos();
        } else {
            System.out.println("ERROR, ESTA TRATANDO DE INSTANCIAR UN OBJETO SINGLETON QUE YA EXISTE");
        }
        return cxObjects;
    }

    @Override
    public ConexionObjetos clone() {
        try {
            throw new CloneNotSupportedException();
        } catch (CloneNotSupportedException e) {

            System.out.println("ESTE OBJETO NO SE PUEDE CLONAR, SOY UNICO - SINGLETON");

        }

        return null;
    }

    public BeanConnector getbConn() {
        return bConn;
    }
}