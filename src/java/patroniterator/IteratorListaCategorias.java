
package patroniterator;

import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;

/**
 *
 * @author D M
 */
public class IteratorListaCategorias {
    // este objeto se encarga de gertionar los agregados a la conexion 
    private LinkedList<Category> listaCategorias = new LinkedList<Category>();
    private int size = 0;
    private static ConexionObjetos conn;
    
    public IteratorListaCategorias(){
        try{
            conn = ConexionObjetos.getInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
        public void add(int id){
            Category categoria = new Category();
            categoria.setId(id);
            
            try{
                ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category WHERE "
                + "id = '" + categoria.getId()+"'");
                while(rs.next()){
                    categoria.setName(rs.getString("name"));
                    
                }
                listaCategorias.add(categoria);
                rs.close();
                
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        public CategoriasIterator iterator(){
            return new CategoriasIterator(listaCategorias);
        }
    }

