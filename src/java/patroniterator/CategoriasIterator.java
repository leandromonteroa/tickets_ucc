
package patroniterator;
 
import java.util.Iterator;
import java.util.LinkedList;
import pojos.Category;

/**
 *
 * @author D M
 */
public class CategoriasIterator implements Iterator<Category>{
    // se define la conexion sobre la cual se realizaran las iteraciones 
    private LinkedList<Category> listaCategorias;
    
    private int counter = 0;
    
    // en el constructor se resive la lista de agregados y se arma en otro objeto concreto
    public CategoriasIterator(LinkedList<Category> listaCategorias){
        this.listaCategorias = listaCategorias;
    }
    
    public Category next(){
        return listaCategorias.get(counter++);
        
    }
    public boolean hasNext(){
        if(counter < listaCategorias.size() && listaCategorias.get(counter) != null){
        return true;
    } else {
            return false;
        }
    }
}
