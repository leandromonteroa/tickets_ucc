package patronprototype;

import java.util.ArrayList;

/**
 *
 * @author D M
 */
public class ClientePrototype {
    public static void main (String args[]) throws CloneNotSupportedException{
        int capacidadLoteTickets = 10;
        PrototipoFacturaTicket prototipoFacturaVentaDental = new FacturaVentaDental("estadio", 6, 001, 25000);
        PrototipoFacturaTicket prototipoFacturaVentaPorMenor = new FacturaVentaPorMenor( "juanchito", 7, 002, 500);
        PrototipoFacturaTicket prototipoFacturaVentaSinIVA = new FacturaVentaSinIVA("plaza de toros", 5, 003, 17000);
        ArrayList listaMediosTickets = new ArrayList();
        for(int i = 0; i < capacidadLoteTickets; i++){
            PrototipoFacturaTicket FacturaVentaDental = (PrototipoFacturaTicket) prototipoFacturaVentaDental.clone();
            FacturaVentaDental.setCodigo(i*001);
            System.out.println(FacturaVentaDental.getLugar() + " / " + FacturaVentaDental.getDuracionMinutos() + 
                    " / " + FacturaVentaDental.getCodigo());
            PrototipoFacturaTicket FacturaVentaPorMenor = (PrototipoFacturaTicket) prototipoFacturaVentaPorMenor.clone();
            FacturaVentaPorMenor.setCodigo(i*002);
            System.out.println(FacturaVentaPorMenor.getLugar()+" / " + FacturaVentaPorMenor.getDuracionMinutos() +
                    " / " + FacturaVentaPorMenor.getCodigo());
            PrototipoFacturaTicket FacturaVentaSinIVA = (PrototipoFacturaTicket) prototipoFacturaVentaSinIVA.clone();
            FacturaVentaSinIVA.setCodigo(i*003);
            System.out.println(FacturaVentaSinIVA.getLugar() + " / " + FacturaVentaSinIVA.getDuracionMinutos() + 
                    " / " + FacturaVentaSinIVA.getCodigo());
        }
    }    
}
