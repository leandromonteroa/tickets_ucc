
package patronprototype;

/**
 *
 * @author D M
 */
public abstract class PrototipoFacturaTicket implements Cloneable {
    private String lugar;
    private int duracionMinutos;
    private int codigo;

    public PrototipoFacturaTicket(String lugar, int duracionMinutos, int codigo){
        this.lugar = lugar;
        this.duracionMinutos = duracionMinutos;
        this.codigo = codigo;
    }
    
    public Object clone() throws CloneNotSupportedException{
        return super.clone();   
    }
    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public int getDuracionMinutos() {
        return duracionMinutos;
    }

    public void setDuracionMinutos(int duracionMinutos) {
        this.duracionMinutos = duracionMinutos;
    }
        public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
}
    

