
package patronprototype;

/**
 *
 * @author D M
 */
public class FacturaVentaSinIVA extends PrototipoFacturaTicket {
    private int participantes;
    private int codigo;
    public FacturaVentaSinIVA (String lugar, int duracionMinutos, int codigo, int participantes ){
        super(lugar, duracionMinutos, codigo);
        this.participantes = participantes;
    }

    public int getParticipantes() {
        return participantes;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    } 
}
    
    

