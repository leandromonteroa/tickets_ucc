package patronfacade;

import connector.ConexionObjetos;
import cruds.CRUDCategorias;
import cruds.CRUDTickets;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;
import pojos.Ticket;

/**
 *
 * @author D M
 */
public class FacadeTickets {
    
    private CRUDTickets crudTickets;
    private CRUDCategorias crudCategorias;
    private ConexionObjetos conn;
    
    public FacadeTickets(){
        crudTickets = new CRUDTickets();
        crudCategorias = new CRUDCategorias();
        conn = new ConexionObjetos();
    }
    
    // se gener ala actividad que se va aproveer al cliente
    // tener cuidado con los id que se propagan entre objetos 
    public LinkedList<TicketsFacade> generarTicketsFacade(){
        LinkedList<TicketsFacade> listaTicketsCategoria = new LinkedList<>();
        LinkedList<Ticket> listaTickets = crudTickets.listarTickets();
        LinkedList<Category> listaCategorias = new LinkedList<>();
        
        // lo ideal es que los datos de cada tabla los obtengo atraves de un crud
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category");
            while(rs.next()){
                Category cat = new Category();
                cat.setId(rs.getInt("id"));
                cat.setName(rs.getString("name"));
                listaCategorias.add(cat);
            }
            
        }catch (Exception e){
            e.printStackTrace();
        }
        for(int x = 0; x < listaTickets.size(); x ++){
            for (int y = 0; y < listaCategorias.size(); y ++){
                if(listaTickets.get(x).getCategory() == listaCategorias.get(y).getId()){
                    listaTickets.get(x).setCategoryName(listaCategorias.get(y).getName());
                    System.out.println("id_categoria >>>> " + listaTickets.get(x).getCategory());
                    System.out.println("name_categoria >>>>" + listaCategorias.get(y).getName());
                    System.out.println("-----------------------------");
                }
            }
            Ticket p = listaTickets.get(x);
            TicketsFacade pf = new TicketsFacade(p.getId(), p.getName(), p.getDescription(),
            p.getPrice(), p.getCategoryName());
            listaTicketsCategoria.add(pf);
        }
        return listaTicketsCategoria;
    }
}
