
package patronfacade;

/**
 *
 * @author D M
 */
public class TicketsFacade {
    private int id;
    private String nameTicket;
    private String descriptionTicket;
    private int priceTicket;
    private String categoryTicket;
    
    // se define el constructor
    public TicketsFacade(int id, String nameTicket, String descriptionTicket,
    int priceTicket, String categoryTicket){
    this.id = id;
    this.nameTicket = nameTicket;
    this.descriptionTicket = descriptionTicket;
    this.priceTicket = priceTicket;
    this.categoryTicket = categoryTicket;
   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameTicket() {
        return nameTicket;
    }

    public void setNameTicket(String nameTicket) {
        this.nameTicket = nameTicket;
    }

    public String getDescriptionTicket() {
        return descriptionTicket;
    }

    public void setDescriptionTicket(String descriptionTicket) {
        this.descriptionTicket = descriptionTicket;
    }

    public int getPriceTicket() {
        return priceTicket;
    }

    public void setPriceTicket(int priceTicket) {
        this.priceTicket = priceTicket;
    }

    public String getCategoryTicket() {
        return categoryTicket;
    }

    public void setCategoryTicket(String categoryTicket) {
        this.categoryTicket = categoryTicket;
    }
    
}
