
package patronbuilder;

import pojos.Ticket;

/**
 *
 * @author D M
 */
public class TicketVendedor {
    private TicketBuilder ticketBuilder;
    
    public void buildTicket(String name, String description, int price, int category){
      ticketBuilder.crearTicketPatronBuilder();
      ticketBuilder.construirTicket(name, description, price, category);
    }
    public void setTicketBuilder(TicketBuilder ticketB){
      ticketBuilder = ticketB;  
    }
    public Ticket getTicketPatronBuilder(){
        return ticketBuilder.getTicketPatronBuilder();
    }

}

   
