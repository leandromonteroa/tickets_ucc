
package patronbuilder;

import cruds.CRUDTickets;
import java.util.LinkedList;
import pojos.Ticket;

/**
 *
 * @author D M
 */
public class TestProductoBuilder {
    private static CRUDTickets crud = new CRUDTickets(); 
    public static void main (String[] args){
        System.out.println("BUILDER --> TICKET PARA GRADERIA >> ");
        LinkedList<Ticket> listaTicketsGraderia = crud.listarTicketsBuilder(4);
        for(int x = 0; x < listaTicketsGraderia.size(); x++){
            System.out.println("----------------------------");
            System.out.println("LEANDRO > " + listaTicketsGraderia.get(x).getName() + " / precio > "
            + listaTicketsGraderia.get(x).getPrice());
        }
        System.out.println("************************");
        System.out.println("BUILDER --> TICKET PARA PLATINO >>> ");
        LinkedList<Ticket> listaTicketsPlatino = crud.listarTicketsBuilder(3);
        for(int y = 0; y < listaTicketsPlatino.size(); y++){
            System.out.println("----------------------------");
            System.out.println("DIDIER > " + listaTicketsPlatino.get(y).getName() + " / precio > "
            + listaTicketsPlatino.get(y).getPrice());
        }
        System.out.println("************************");
        System.out.println("BUILDER --> TICKET PARA GENERAL");
        LinkedList<Ticket> listaTicketsGeneral = crud.listarTicketsBuilder(2);
        for(int z =0; z < listaTicketsGeneral.size(); z++){
            System.out.println("----------------------------");
            System.out.println("XIOMARA > " + listaTicketsGeneral.get(z).getName() + " / precio> "
            + listaTicketsGeneral.get(z).getPrice());
        }
    }
}
