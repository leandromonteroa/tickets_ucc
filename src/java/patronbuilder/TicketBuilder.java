
package patronbuilder;

import pojos.Ticket;

/**
 *
 * @author D M
 */
public abstract class TicketBuilder {
    protected Ticket TicketB;
    
    public Ticket getTicketPatronBuilder(){
    return TicketB;    
    }
    public void crearTicketPatronBuilder(){
    TicketB = new Ticket();    
    }
    public abstract void construirTicket(String name, String description, int price, int category );
}
