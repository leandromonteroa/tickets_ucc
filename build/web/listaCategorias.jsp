<%-- 
    Document   : listaCategorias
    Created on : 22/11/2020, 05:40:50 PM
    Author     : D M
--%>

<%@page import="pojos.Category"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="java.util.LinkedList"%>
<%@page import="pojos.Ticket"%>
<%@page import="cruds.CRUDTickets"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Categorias</title>
        
        <h1 style="border:2px solid black;">Lista Categoria Tickets</h1>
        <style>
body {
  background: url('img/tt.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed; 
  background-size: 100% 100%;
}
</style>
    </head>
    <body>
        <br>
        <table border ="1">
            <tr>
                <td>
                    <h1 style="background-color:gray;">LISTA TICKETS CATEGORIA GENERAL >>> PATRÓN BUILDER</h1>
                    <table border ="1">
                        <tr>
                            <td>codigo ticket</td>
                            <td>nombre</td>
                            <td>descripcion</td>
                            <td>precio</td>
                            <td>categoria</td>
                        </tr>
                        <%
                            CRUDTickets crudTickets = new CRUDTickets();
                            LinkedList<Ticket> listaTicketsGeneral = crudTickets.listarTicketsBuilder(2);
                            for (int i = 0; i < listaTicketsGeneral.size(); i++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getId() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getName() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </td>
            </tr>
            <tr>
                <td> 
                    <h1 style="background-color:gray;">LISTA TICKETS CATEGORIA GRADERIA >>> PATRÓN BUILDER </h1>
                     
                    <table border ="1">
                        <tr>
                            <td>codigo ticket</td>
                            <td>nombre</td>
                            <td>descripcion</td>
                            <td>precio</td>
                            <td>categoria</td>
                        </tr>
                        <%
                            LinkedList<Ticket> listaTicketsGraderia = crudTickets.listarTicketsBuilder(4);
                            for (int x = 0; x < listaTicketsGraderia.size(); x++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getId() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getName() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h1 style="background-color:gray;">LISTA TICKETS PLATINO >>> PATRÓN BUILDER</h1>
                    
                    <table border ="1">
                        <tr>
                            <td>codigo ticket</td>
                            <td>nombre</td>
                            <td>descripcion</td>
                            <td>precio</td>
                            <td>categoria</td>
                        </tr>
                        <%
                            LinkedList<Ticket> listaTicketsPlatino = crudTickets.listarTicketsBuilder(3);
                            for (int y = 0; y < listaTicketsPlatino.size(); y++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getId() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getName() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        %>    
                            
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h1 style="background-color:gray;">LISTA JOYERIAS >>> PATRON FACADE</h1>
                    
                    <table border ="1">
                        <tr>
                            <td>codigo ticket</td>
                            <td>nombre</td>
                            <td>descripcion</td> 
                            <td>precio</td>
                           
                        
                            <td>nombre_categoria</td>
                        </tr>
                        <%
                            CRUDCategorias crudCategorias = new CRUDCategorias();
                            LinkedList<Ticket> listaTickets = crudTickets.listarTickets();
                            LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();

                            for (int x = 0; x < listaTickets.size(); x++) {
                                for (int y = 0; y < listaCategorias.size(); y++) {
                                    if (listaTickets.get(x).getCategory() == listaCategorias.get(y).getId()) {
                                        listaTickets.get(x).setCategoryName(listaCategorias.get(y).getName());
                                        Ticket ticket = listaTickets.get(x);
                                        Category categoria = listaCategorias.get(y);
                                        out.println("<tr>");
                                        out.println("<td>" + ticket.getId() + "</td>");
                                        out.println("<td>" + ticket.getName() + "</td>");
                                        out.println("<td>" + ticket.getDescription() + "</td>");
                                        out.println("<td>" + ticket.getPrice() + "</td>");
                                        out.println("<td>" + categoria.getName() + "</td>");
                                        out.println("</tr>");
                                    }
                                }
                            }
                        %>
                    </table>   
                </td>
            </tr>
        </table>
        </div>            
        <br>
        <hr>
        <a href="tickets.jsp">Volver al inicio</a>
        <hr>
    </body>
</html>


            
       