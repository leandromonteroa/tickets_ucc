package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import pojos.Category;
import cruds.CRUDCategorias;
import java.util.LinkedList;
import pojos.Ticket;
import cruds.CRUDTickets;

public final class listaCategorias_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Lista Categorias</title>\n");
      out.write("        \n");
      out.write("        <h1 style=\"border:2px solid black;\">Lista Categoria Tickets</h1>\n");
      out.write("        <style>\n");
      out.write("body {\n");
      out.write("  background: url('img/tt.jpg');\n");
      out.write("  background-repeat: no-repeat;\n");
      out.write("  background-attachment: fixed; \n");
      out.write("  background-size: 100% 100%;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <br>\n");
      out.write("        <table border =\"1\">\n");
      out.write("            <tr>\n");
      out.write("                <td>\n");
      out.write("                    <h1 style=\"background-color:gray;\">LISTA TICKETS CATEGORIA GENERAL >>> PATRÓN BUILDER</h1>\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo ticket</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td>\n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            CRUDTickets crudTickets = new CRUDTickets();
                            LinkedList<Ticket> listaTicketsGeneral = crudTickets.listarTicketsBuilder(2);
                            for (int i = 0; i < listaTicketsGeneral.size(); i++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getId() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getName() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsGeneral.get(i).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td> \n");
      out.write("                    <h1 style=\"background-color:gray;\">LISTA TICKETS CATEGORIA GRADERIA >>> PATRÓN BUILDER </h1>\n");
      out.write("                     \n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo ticket</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td>\n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Ticket> listaTicketsGraderia = crudTickets.listarTicketsBuilder(4);
                            for (int x = 0; x < listaTicketsGraderia.size(); x++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getId() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getName() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsGraderia.get(x).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td>\n");
      out.write("                    <h1 style=\"background-color:gray;\">LISTA TICKETS PLATINO >>> PATRÓN BUILDER</h1>\n");
      out.write("                    \n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo ticket</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td>\n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Ticket> listaTicketsPlatino = crudTickets.listarTicketsBuilder(3);
                            for (int y = 0; y < listaTicketsPlatino.size(); y++){
                                out.println("<tr>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getId() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getName() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getDescription() + "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getPrice()+ "</td>");
                                out.println("<td>" + listaTicketsPlatino.get(y).getCategory()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("    \n");
      out.write("                            \n");
      out.write("                    </table>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td>\n");
      out.write("                    <h1 style=\"background-color:gray;\">LISTA JOYERIAS >>> PATRON FACADE</h1>\n");
      out.write("                    \n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo ticket</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                            <td>id_categoria</td>\n");
      out.write("                            <td>nombre_categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            CRUDCategorias crudCategorias = new CRUDCategorias();
                            LinkedList<Ticket> listaTickets = crudTickets.listarTickets();
                            LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();

                            for (int x = 0; x < listaTickets.size(); x++) {
                                for (int y = 0; y < listaCategorias.size(); y++) {
                                    if (listaTickets.get(x).getCategory() == listaCategorias.get(y).getId()) {
                                        listaTickets.get(x).setCategoryName(listaCategorias.get(y).getName());
                                        Ticket ticket = listaTickets.get(x);
                                        Category categoria = listaCategorias.get(y);
                                        out.println("<tr>");
                                        out.println("<td>" + ticket.getId() + "</td>");
                                        out.println("<td>" + ticket.getName() + "</td>");
                                        out.println("<td>" + ticket.getDescription() + "</td>");
                                        out.println("<td>" + ticket.getPrice() + "</td>");
                                        out.println("<td>" + ticket.getCategory() + "</td>");
                                        out.println("<td>" + categoria.getName() + "</td>");
                                        out.println("</tr>");
                                    }
                                }
                            }
                        
      out.write("\n");
      out.write("                    </table>   \n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("        </table>\n");
      out.write("        </div>            \n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"tickets.jsp\">Volver al inicio</a>\n");
      out.write("        <hr>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
      out.write("            \n");
      out.write("       ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
