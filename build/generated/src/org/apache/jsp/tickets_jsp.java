package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Iterator;
import pojos.Category;
import cruds.CRUDCategorias;
import patroniterator.IteratorListaCategorias;
import java.util.LinkedList;
import pojos.Ticket;
import cruds.CRUDTickets;

public final class tickets_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("       <style>\n");
      out.write("body {\n");
      out.write("  background: url('img/tt.jpg');\n");
      out.write("  background-repeat: no-repeat;\n");
      out.write("  background-attachment: fixed; \n");
      out.write("  background-size: 100% 100%;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h1 style=\"border:2px solid black;\">Tickets Disponibles</h1>\n");
      out.write("        <table border = \"1\"> \n");
      out.write("            <tr>\n");
      out.write("               <td>identificativo</td> \n");
      out.write("               <td>nombre ticket</td>\n");
      out.write("               <td>descripcion</td>    \n");
      out.write("               <td>precio</td> \n");
      out.write("               <td>categoria</td> \n");
      out.write("            </tr>\n");
      out.write("                ");

                CRUDTickets crudTickets = new CRUDTickets();    
                LinkedList<Ticket> lista = crudTickets.listarTickets();
                for(int i =0 ;i < lista.size(); i++){
                    Ticket ticket = lista.get(i);
                    out.println("<tr>");
                    out.println("<td>" + ticket.getId() + "</td>");
                    out.println("<td>" + ticket.getName() + "</td>");
                    out.println("<td>" + ticket.getDescription() + "</td>");
                    out.println("<td>" + ticket.getPrice() + "</td>");
                    out.println("<td>" + ticket.getCategory() + "</td>");
                    out.println("</tr>");
                }
                
      out.write("\n");
      out.write("        </table>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("        <br>\n");
      out.write("        <form name =\"formulariotickets\" action=\"ControladorTickets\"method=\"get\">\n");
      out.write("            codigo > <input type=\"text\" name=\"id\"><br>\n");
      out.write("            nombre ticket > <input type=\"text\" name=\"name\"><br>\n");
      out.write("            descripcion > <input type=\"text\" name=\"description\"><br>\n");
      out.write("            precio > <input type=\"text\" name=\"price\" value=\"0\"><br>\n");
      out.write("            patron iterator >>>> <br>\n");
      out.write("            categoria > \n");
      out.write("            \n");
      out.write("            <select name=\"category\">\n");
      out.write("                ");

                    CRUDCategorias crudCategorias = new CRUDCategorias();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                    for (int x = 0; x < listaCat.size(); x++){
                        listaCategorias.add(listaCat.get(x).getId());
                    }
                    Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                    while(iteratorCategorias.hasNext()){
                        Category categoria = (Category)iteratorCategorias.next();
                        out.println("<option value = \"" + categoria.getId() + "\">" +
                                categoria.getName() + "</option>");
                    }   
                            
                
      out.write("\n");
      out.write("            </select>\n");
      out.write("            \n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"insertarticket\" value=\"guardar ticket\" style=\"background-color:gray;\">\n");
      out.write("            <input type=\"submit\" name=\"editarticket\" value=\"actualizar ticket\" style=\"background-color:gray;\">\n");
      out.write("            <input type=\"submit\" name=\"eliminarticket\" value=\"borrar ticket\" style=\"background-color:gray;\">\n");
      out.write("            </h1>\n");
      out.write("        </form>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"listaCategorias.jsp\">Listas</a>\n");
      out.write("        <hr>\n");
      out.write("        <hr>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
