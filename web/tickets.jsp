<%-- 
    Document   : tickets
    Created on : 10/11/2020, 03:56:48 PM
    Author     : D M
--%>

<%@page import="java.util.Iterator"%>
<%@page import="pojos.Category"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="patroniterator.IteratorListaCategorias"%>
<%@page import="java.util.LinkedList"%>
<%@page import="pojos.Ticket"%>
<%@page import="cruds.CRUDTickets"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        
       <style>
body {
  background: url('img/tt.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed; 
  background-size: 100% 100%;
}
</style>
    </head>
    <body>
        <h1 style="border:2px solid black;">Tickets Disponibles</h1>
        <table border = "1"> 
            <tr>
               <td>identificativo</td> 
               <td>nombre ticket</td>
               <td>descripcion</td>    
               <td>precio</td> 
               <td>categoria</td> 
            </tr>
                <%
                CRUDTickets crudTickets = new CRUDTickets();    
                LinkedList<Ticket> lista = crudTickets.listarTickets();
                for(int i =0 ;i < lista.size(); i++){
                    Ticket ticket = lista.get(i);
                    out.println("<tr>");
                    out.println("<td>" + ticket.getId() + "</td>");
                    out.println("<td>" + ticket.getName() + "</td>");
                    out.println("<td>" + ticket.getDescription() + "</td>");
                    out.println("<td>" + ticket.getPrice() + "</td>");
                    out.println("<td>" + ticket.getCategory() + "</td>");
                    out.println("</tr>");
                }
                %>
        </table>
        <br>
        <hr>
        <br>
        <form name ="formulariotickets" action="ControladorTickets"method="get">
            codigo > <input type="text" name="id"><br>
            nombre ticket > <input type="text" name="name"><br>
            descripcion > <input type="text" name="description"><br>
            precio > <input type="text" name="price" value="0"><br>
            patron iterator >>>> <br>
            categoria > 
            
            <select name="category">
                <%
                    CRUDCategorias crudCategorias = new CRUDCategorias();
                    IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                    LinkedList<Category> listaCat = crudCategorias.listarCategorias();
                    
                    for (int x = 0; x < listaCat.size(); x++){
                        listaCategorias.add(listaCat.get(x).getId());
                    }
                    Iterator<Category> iteratorCategorias = listaCategorias.iterator();
                    while(iteratorCategorias.hasNext()){
                        Category categoria = (Category)iteratorCategorias.next();
                        out.println("<option value = \"" + categoria.getId() + "\">" +
                                categoria.getName() + "</option>");
                    }   
                            
                %>
            </select>
            
            <hr>
            <br>
            <input type="submit" name="insertarticket" value="guardar ticket" style="background-color:gray;">
            <input type="submit" name="editarticket" value="actualizar ticket" style="background-color:gray;">
            <input type="submit" name="eliminarticket" value="borrar ticket" style="background-color:gray;">
            </h1>
        </form>
        <br>
        <hr>
        <a href="listaCategorias.jsp">Listas</a>
        <hr>
        <hr>
    </body>
</html>